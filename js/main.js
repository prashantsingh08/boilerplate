var Vehicle = Backbone.Model.extend({
    urlRoot: '/api/vehicles',
    idAttribute: 'registrationNumber',
    validate: function(attr) {
        if(!attr.registrationNumber) {
            return 'Vehicle Registration Number required.'
        }
    },
    start: function() {
        console.log('Vehicle Start');
    }
});

var Car = Vehicle.extend({
    start: function() {
        Vehicle.prototype.start.apply(this);
        console.log(`Car with registration number ${this.get("registrationNumber")} started.`);
    }
});
var car = new Car({
    registrationNumber: 'XLI887',
    color: 'Blue',
});
car.start();